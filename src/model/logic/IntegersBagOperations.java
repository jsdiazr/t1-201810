package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int totalValue(IntegersBag bag){
		int total = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				total += iter.next();
			}
		}
		return total;
	}
	
	public int outputRandom(IntegersBag bag){
		int value;
		int total = 0;
		ArrayList<Integer> list = new ArrayList<Integer>();
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				list.add(value);
				total ++;
			}
		}
		
		int randomNumber;
		randomNumber = (int) (Math.random() * (total));
		int answer = list.get(randomNumber);
		return answer;
	}
	
	public ArrayList<Integer> valueList(IntegersBag bag){
		ArrayList<Integer> list = new ArrayList<Integer>();
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				list.add(iter.next());
			}
		}
		return list;
	}
	
}
