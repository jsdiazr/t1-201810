package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static int getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	public static int totalValue(IntegersBag bag){
		return model.totalValue(bag);
	}
	
	public static int outputRandom(IntegersBag bag){
		return model.outputRandom(bag);
	}
	
	public static ArrayList<Integer> valueList(IntegersBag bag){
		return model.valueList(bag);
	}
	
}
